package main

import "fmt"

func main() {
	const (
		USD  = 0.000070
		Euro = 0.000059
		/*
			- 1 GBP = 100 Knuts
			- 1 Galleon = 17 Sickle
			- 1 Sickle = 29 Knut
		*/
		Knut    = 100
		Sickle  = 29
		Galleon = 493
	)
	var pilihan, uang int
	fmt.Println("=== Program Konversi Mata Uang ===")
	fmt.Println("Menu pilihan: ")
	fmt.Println("1. Rupiah ke Dollar")
	fmt.Println("2. Rupiah ke Euro")
	fmt.Println("3. GBP ke Knut, Sickle, Galleon ")
	fmt.Println("Pilih: ")
	fmt.Scan(&pilihan)
	if pilihan == 1 {
		fmt.Print("Masukkan jumlah uang rupiah: ")
		fmt.Scan(&uang)
		hitung := float64(uang) * USD
		fmt.Printf("Jumlah dollar yang didapat: %.2f\n", hitung)
	} else if pilihan == 2 {
		fmt.Print("Masukkan jumlah uang rupiah: ")
		fmt.Scan(&uang)
		hitung := float64(uang) * Euro
		fmt.Printf("Jumlah euro yang didapat: %.2f\n", hitung)
	} else if pilihan == 3 {
		fmt.Print("Masukkan jumlah uang GBP: ")
		fmt.Scan(&uang)
		hitung_knut := uang * Knut
		hitung_galleon := hitung_knut / Galleon
		sisa_knut := hitung_knut % Galleon
		sisa_sickle := sisa_knut / Sickle
		fmt.Printf("Jumlah knut yang didapat : %d\n", hitung_knut)
		fmt.Printf("Hasil penukaran mendapatkan : %d Galleon(s)\n", hitung_galleon)
		fmt.Printf("Sisa ditukar menjadi : %d Sickle(s)\n", sisa_sickle)
		fmt.Printf("Keping knut yang tersisa : %d Knut(s)\n", sisa_knut)

	} else {
		fmt.Println("Menu pilihan tidak tersedia")
	}
	fmt.Println("\nProgram berakhir...")
}
